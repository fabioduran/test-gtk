#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from dlg_auxiliar import DlgAuxiliar

class ventana(Gtk.ApplicationWindow):

    def __init__(self,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        builder = Gtk.Builder()
        builder.add_from_file("ui/ejemplo-gtk.glade")
        
        box = builder.get_object("box1")
        self.add(box)
        
        # text = builder.get_object("")
        self.resize(400, 400)
        self.set_title("Hola Mundo")

        button = builder.get_object("boton1")
        button.set_label("Abre dialogo")
        button.connect("clicked", self.boton1_clicked)

        self.texto = ""

    def boton1_clicked(self, btn=None):
        print("Click")
        open_dlg = DlgAuxiliar()
        open_dlg.prueba_metodo()
        dialogo = open_dlg.dialogo

        if self.texto != "":
            open_dlg.texto.set_text(self.texto)

        response = dialogo.run()

        if response == Gtk.ResponseType.CANCEL or response == Gtk.ResponseType.DELETE_EVENT:
            dialogo.destroy()
        else:
            print("Hola")
            self.texto = open_dlg.texto.get_text()

            dialogo.destroy()
            self.boton1_clicked(self)


        
