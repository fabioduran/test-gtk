from subprocess import Popen

commands = [
    'echo 1; sleep 2',
    'echo 2; sleep 2',
    'echo 3; sleep 2',
    'echo 4; sleep 2',
    'echo 5; sleep 2',   
]
# run in parallel
processes = [Popen(cmd, shell=True) for cmd in commands]
# do other things here..
# wait for completion
for p in processes: 
    p.wait()

print("FIN")
