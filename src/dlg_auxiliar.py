#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class DlgAuxiliar():

    def __init__(self, *args, **kargs):
        builder = Gtk.Builder()
        builder.add_from_file("ui/ejemplo-gtk.glade")

        self.dialogo = builder.get_object("dlg_auxiliar")
        self.dialogo.add_buttons(Gtk.STOCK_OK, Gtk.ResponseType.OK,
                                 Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)

        self.texto = builder.get_object("texto")
        # dlg.show_all()

        # boton_cancelar = builder.get_object("cancelar")
        # boton_cancelar.connect("clicked", self.boton_cancelar_clicked)



    def boton_cancelar_clicked(self, btn=None):
        # self.dialogo.destroy()
        return Gtk.ResponseType.CANCEL


    def prueba_metodo(self):
        print("Soy un método de la clase")
