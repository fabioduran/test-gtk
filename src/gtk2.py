#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from ventana import ventana
        
class Application(Gtk.Application):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="org.example.myapp",
                         # flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **kwargs)
        self.window = None

    def do_startup(self):
        print("do_startup")
        Gtk.Application.do_startup(self)
    
    def do_activate(self):
        print("do_activate")
        if not self.window:
            self.window = ventana(application=self, title="Main Window")
            
        self.window.present()    
    
if __name__ == "__main__":
    a = Application()
    a.run()
