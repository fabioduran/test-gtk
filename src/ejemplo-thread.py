import logging
import threading
import time
import subprocess

def thread_function(name):
    # logging.info("Thread %s: starting", name)
    subprocess.run(args=["sleep", "10"])
    # time.sleep(10)
    logging.info("Thread %s: finishing", name)

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    threads = list()
    for index in range(10):
        logging.info("Main    : create and start thread %d.", index)
        x = threading.Thread(target=thread_function, args=(index,))
        threads.append(x)
        x.start()

    
    for i in threads:
        i.join()
        print("a")
    
    print("Termino")
    # for index, thread in enumerate(threads):
    #     logging.info("Main    : before joining thread %d.", index)
    #     print("1")
    #     print("2")
    #     logging.info("Main    : thread %d done", index)
